import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import { Navbar } from "./app/Navbar";
import { SingleShow } from "./features/show/SingleShow";
import { EpisodesList } from "./features/episodes/EpisodesList";
import { SingleEpisode } from "./features/episodes/SingleEpisode";
import { ThemeProvider } from "styled-components";
import { theme } from "./theme/theme";
import GlobalStyle from "./theme/GlobalStyle";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Router>
        <Navbar />
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <>
                <SingleShow />
                <EpisodesList />
              </>
            )}
          />
          <Route exact path="/episodes/:episodeId" component={SingleEpisode} />
          <Redirect to="/" />
        </Switch>
      </Router>
    </ThemeProvider>
  );
}

export default App;
