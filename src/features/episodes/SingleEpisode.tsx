import React from "react";
import { useSelector } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import { RootState } from "../../app/store";
import { Responsive } from "../../styled/responsive";
import { removeTags } from "../../utils/removeTags";
import { selectEpisodeById } from "./episodesSlice";

export const SingleEpisode = ({
  match,
}: RouteComponentProps<{ episodeId: string }>) => {
  const { episodeId } = match.params;

  const episode = useSelector((state: RootState) =>
    selectEpisodeById(state, episodeId)
  );

  if (!episode) {
    return (
      <section>
        <p>Episode not found</p>
      </section>
    );
  }

  return (
    <section>
      <article>
        <p>{episode.name}</p>
        <Responsive>
          <img src={episode.image.medium} alt={episode.name} />
          <p>{removeTags(episode.summary)}</p>
        </Responsive>
      </article>
    </section>
  );
};
