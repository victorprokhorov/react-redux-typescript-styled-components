import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import { client } from "../../api/client";
import { RootState } from "../../app/store";
import { Episode } from "./episode";
import { State } from "./state";

export const fetchEpisodes = createAsyncThunk(
  "episodes/fetchEpisodes",
  async () =>
    await client<Episode[]>("https://api.tvmaze.com/shows/1955/episodes")
);

const initialState: State = {
  episodes: [],
  status: "idle",
};

const episodesSlice = createSlice({
  name: "episodes",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchEpisodes.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchEpisodes.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.episodes = state.episodes.concat(action.payload);
      });
  },
});

export default episodesSlice.reducer;

export const selectAllEpisodes = (state: RootState) => state.episodes.episodes;

export const selectEpisodeById = (state: RootState, episodeId: string) =>
  state.episodes.episodes.find(
    (episode: Episode) => episode.id === Number(episodeId)
  );
