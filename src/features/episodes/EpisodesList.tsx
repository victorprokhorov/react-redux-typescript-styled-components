import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchEpisodes, selectAllEpisodes } from "./episodesSlice";
import { RootState } from "../../app/store";
import { Link } from "react-router-dom";

export const EpisodesList = () => {
  const dispatch = useDispatch();
  const episodes = useSelector(selectAllEpisodes);
  const episodeStatus = useSelector(
    (state: RootState) => state.episodes.status
  );

  useEffect(() => {
    if (episodeStatus === "idle") {
      dispatch(fetchEpisodes());
    }
  }, [dispatch, episodeStatus]);

  let content;

  if (episodeStatus === "loading") {
    content = <p>Loading.</p>;
  } else if (episodeStatus === "succeeded") {
    content = episodes.map((episode) => (
      <Link key={episode.id} to={`/episodes/${episode.id}`}>
        {episode.name}
      </Link>
    ));
  }

  return <section>{content}</section>;
};
