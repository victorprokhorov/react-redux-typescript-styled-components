export interface State {
  episodes: Episode[];
  status: "idle" | "loading" | "succeeded";
}
