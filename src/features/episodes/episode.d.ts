export interface Episode {
  id: number;
  url: string;
  name: string;
  season: number;
  number: number;
  type: Type;
  airdate: Date;
  airtime: string;
  airstamp: Date;
  runtime: number;
  image: Image;
  summary: string;
  _links: Links;
}

export interface Links {
  self: Self;
}

export interface Self {
  href: string;
}

export interface Image {
  medium: string;
  original: string;
}

export enum Type {
  Regular = "regular",
}
