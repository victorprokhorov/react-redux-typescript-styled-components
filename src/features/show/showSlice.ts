import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { client } from "../../api/client";
import { Show } from "./show";
import { State } from "./state";

const initialState: State = {
  show: {},
  status: "idle",
};

export const fetchShow = createAsyncThunk(
  "episodes/fetchShow",
  async () => await client<Show>("https://api.tvmaze.com/shows/1955")
);

const usersSlice = createSlice({
  name: "users",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchShow.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchShow.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.show = action.payload;
        console.log(action.payload);
      });
  },
});

export default usersSlice.reducer;
