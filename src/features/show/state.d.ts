export interface State {
  show: Show;
  status: "idle" | "loading" | "succeeded";
}
