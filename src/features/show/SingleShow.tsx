import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchShow } from "./showSlice";
import { RootState } from "../../app/store";
import { removeTags } from "../../utils/removeTags";
import { Responsive } from "../../styled/responsive";

export const SingleShow = () => {
  const dispatch = useDispatch();
  const show = useSelector((state: RootState) => state.show.show);
  const showStatus = useSelector((state: RootState) => state.show.status);

  useEffect(() => {
    if (showStatus === "idle") {
      dispatch(fetchShow());
    }
  }, [dispatch, showStatus]);

  let content;

  if (showStatus === "loading") {
    content = <p>Loading.</p>;
  } else if (showStatus === "succeeded") {
    content = (
      <>
        <p>{show.title}</p>
        <Responsive>
          <img src={show.image.medium} alt={show.name} />
          <p>{removeTags(show.summary)}</p>
        </Responsive>
      </>
    );
  }

  return <section>{content}</section>;
};
