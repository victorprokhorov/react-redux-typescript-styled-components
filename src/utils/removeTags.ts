export function removeTags(string: string) {
  if (string === null || string === "") {
    return false;
  } else {
    string = string.toString();
  }
  return string.replace(/(<([^>]+)>)/gi, "");
}
