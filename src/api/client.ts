export async function client<T>(endpoint: string) {
  let data: T;
  try {
    const response = await fetch(endpoint, {
      method: "GET",
    });
    data = await response.json();
    if (response.ok) {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    return Promise.reject(err.message);
  }
}
