import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    a,
    abbr,
    acronym,
    address,
    applet,
    article,
    aside,
    audio,
    b,
    big,
    blockquote,
    body,
    canvas,
    caption,
    center,
    cite,
    code,
    dd,
    del,
    details,
    dfn,
    div,
    dl,
    dt,
    em,
    embed,
    fieldset,
    figcaption,
    figure,
    footer,
    form,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    header,
    hgroup,
    html,
    i,
    iframe,
    img,
    ins,
    kbd,
    label,
    legend,
    li,
    mark,
    menu,
    nav,
    object,
    ol,
    output,
    p,
    pre,
    q,
    ruby,
    s,
    samp,
    section,
    small,
    span,
    strike,
    strong,
    sub,
    summary,
    sup,
    table,
    tbody,
    td,
    tfoot,
    th,
    thead,
    time,
    tr,
    tt,
    u,
    ul,
    var,
    video {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    }
    article,
    aside,
    details,
    figcaption,
    figure,
    footer,
    header,
    hgroup,
    menu,
    nav,
    section {
    display: block;
    }
    body {
    line-height: 1;
    }
    ol,
    ul {
    list-style: none;
    }
    blockquote,
    q {
    quotes: none;
    }
    blockquote:after,
    blockquote:before,
    q:after,
    q:before {
    content: "";
    content: none;
    }
    table {
    border-collapse: collapse;
    border-spacing: 0;
    }
    a,
    a:visited {
    color: inherit;
    text-decoration: inherit;
    }
    body {
    background-color: white;
    }
    * {
    color: black;
    }
    section {
    display: flex;
    flex-direction: column;
    }
    * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-size: 100%;
    font-weight: inherit;
  }
  body {
    width: 100%;
    font-family: sans-serif;
    font-size: 5.5vw;
    line-height: 5.5vw;
    font-weight: bold;
  }

  * {
    font: inherit;
    line-height: inherit;
    letter-spacing: inherit;
    padding: 2.2vw 10vw 3.3vw 3vw;
  }
  a {
    text-decoration: underline;
    color: inherit;
    cursor: pointer;
  }

  .text a:hover {
    text-decoration: none;
  }

  .text a:focus {
    text-decoration: none;
    outline: solid 4px rgba(0,0,0,.4);
  }

    ::selection {
    background: #000;
    background: rgba(0,0,0,.99);
    color: #fff;
  }
    img {
      height: 300px;
      width: auto;
      margin: 5px;
    }
    @media (max-width: 1200px) {
      img {
        width: 200px;
        height: auto;
      }
    }
`;
