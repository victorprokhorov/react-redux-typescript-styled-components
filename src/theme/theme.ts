import { DefaultTheme } from "styled-components";

export const theme: DefaultTheme = {
  primaryColor: "black",
  accentColor: "white",
  fontFamily: "Helvetica",
  fontSize: "1rem",
};
