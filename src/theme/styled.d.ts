import "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    fontFamily: string;
    fontSize: string;
    primaryColor: string;
    accentColor: string;
  }
}
