import { configureStore } from "@reduxjs/toolkit";

import showReducer from "../features/show/showSlice";
import episodesReducer from "../features/episodes/episodesSlice";

export const store = configureStore({
  reducer: {
    show: showReducer,
    episodes: episodesReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
