import React from "react";

import { Link } from "react-router-dom";

export const Navbar = () => {
  return (
    <nav>
      <section>
        <Link to="/">
          <p>The Powerpuff Girls</p>
        </Link>
      </section>
    </nav>
  );
};
